package org.example.minidemo;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import org.example.minidemo.dummy.DummyContent;

/**
 * A fragment representing a list of Items.
 */
public class StandardItemFragment extends ListFragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StandardItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setListAdapter(new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS));
    }
}
