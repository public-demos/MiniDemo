package org.example.minidemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {
    private static final String TAG = ItemAdapter.class.getSimpleName();

    private final Context mContext;
    private final String[] mData;

    public ItemAdapter(Context mContext, String[] data) {
        this.mContext = mContext;
        this.mData = data;
    }

    @Override
    public int getCount() {
        return mData.length;
    }

    @Override
    public Object getItem(int position) {
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) LayoutInflater.from(mContext);
        View rowView = inflater.inflate(R.layout.item_row, parent, false);

        TextView text1View = (TextView) rowView.findViewById(R.id.text1_field);
        TextView text2View = (TextView) rowView.findViewById(R.id.text2_field);
        EditText editText = (EditText) rowView.findViewById(R.id.edit_field);

        text1View.setText(mData[position]);
        text2View.setText(mData[position]);
        editText.setText(Integer.toString(position));

        return rowView;
    }
}
