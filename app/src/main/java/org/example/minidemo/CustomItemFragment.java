package org.example.minidemo;

import android.app.ListFragment;
import android.os.Bundle;

/**
 * A fragment representing a list of Items.
 */
public class CustomItemFragment extends ListFragment {

    String[] DATA = new String[] {"foo1", "foo2", "foo3"};

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CustomItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ItemAdapter adapter = new ItemAdapter(this.getActivity(), DATA);
        setListAdapter(adapter);
    }

}
